from migration.db_connection import DbConnections


class PostgresToAwm(DbConnections):

    def migrate(self):
        self.migrate_discipline()
        self.migrate_employees()
        self.migrate_students()

    def migrate_employees(self):
        print('postgres - migrating employees')
        try:
            self.oracle.cursor().execute("""
                alter table EMPLOYEE
                ADD UNIVERSITY_NAME varchar(100) NULL 
            """)
        except Exception:
            pass
        cursor = self.postgres.cursor()
        cursor.execute('select * from employee')
        for row in cursor:
            self.oracle.cursor().execute(f"""
                update employee SET
                university_name = '{row[5]}'
                where ID = {row[0]}
            """)
        cursor.close()

    def migrate_students(self):
        print('postgres - migrating students')
        try:
            self.oracle.cursor().execute("""
                alter table student
                ADD EDUCATION_STANDARD_TYPE varchar(20) NULL CHECK (EDUCATION_STANDARD_TYPE IN ('старый','новый', NULL))
            """)
        except Exception:
            pass
        cursor = self.postgres.cursor()
        cursor.execute('select * from student')
        for row in cursor:
            self.oracle.cursor().execute(f"""
                update STUDENT SET
                EDUCATION_STANDARD_TYPE = '{row[4]}'
                where ID = {row[0]}
            """)
        cursor.close()

    def migrate_discipline(self):
        print('postgres - migrating disciplines')
        try:
            self.oracle.cursor().execute("""
                alter table DISCIPLINE 
                ADD SEMESTER NUMBER(5) NULL
                ADD LECTURES_HOURS NUMBER(5) NULL
                ADD PRACTICE_HOURS NUMBER(5) NULL
                ADD LAB_HOURS NUMBER(5) NULL
                ADD CONTROL_FORM VARCHAR2(200) NULL
            """)
        except Exception:
            pass
        cursor = self.postgres.cursor()
        cursor.execute('select * from discipline')
        for row in cursor.fetchall():
            self.oracle.cursor().execute(f"""
                update DISCIPLINE SET 
                SEMESTER = {row[2]},
                LECTURES_HOURS = {row[3]}, 
                PRACTICE_HOURS = {row[4]},
                LAB_HOURS = {row[5]},
                CONTROL_FORM = '{row[6]}'
                where ID = {row[0]}
            """)
        cursor.close()
