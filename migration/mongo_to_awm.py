from migration.db_connection import DbConnections, format_date


class MongoToAwm(DbConnections):

    def migrate(self):
        self.migrate_dormitory()
        self.migrate_room()
        self.migrate_resident()
        self.migrate_student()

    def migrate_student(self):
        try:
            self.oracle.cursor().execute("""
                alter table student
                ADD has_benefits VARCHAR2(10) DEFAULT 'False'
            """)
        except Exception:
            pass
        for student in self.mongo.mongodb.student.find({"employee_id": {"$ne": None}}):
            self.oracle.cursor().execute(f"""
                update student SET
                has_benefits = '{student['has_benefits']}'
                where employee = {student['employee_id']}
            """)

    def migrate_dormitory(self):
        try:
            self.oracle.cursor().execute("""
                CREATE TABLE dormitory(
                    ID NUMBER(5) PRIMARY KEY CHECK(ID>=0),
                    DORMITORY_LOCATION VARCHAR2(1000),
                    ROOMS_NUMBER NUMBER(5),
                    HAS_BEDBUGS VARCHAR2(10)
                )
            """)
        except Exception:
            pass
        for dormitory in self.mongo.mongodb.dormitory.find():
            try:
                self.oracle.cursor().execute(f"""
                    INSERT INTO DORMITORY VALUES (
                        {dormitory['_id']},
                        '{dormitory['dormitory_location']}',
                        {dormitory['rooms_number']},
                        '{dormitory['has_bedbugs']}'
                    )
                """)
            except Exception:
                pass

    def migrate_room(self):
        try:
            self.oracle.cursor().execute("""
                CREATE TABLE ROOM(
                    ID NUMBER(5) PRIMARY KEY CHECK(ID>=0),
                    dormitory NUMBER(5) NOT NULL CONSTRAINT room_dormitory_fkey REFERENCES dormitory(ID),
                    payment_amount NUMBER(5),
                    room_number NUMBER(5),
                    room_occupancy NUMBER(5),
                    resident_registered NUMBER(5),
                    disinfection_date DATE
                )
            """)
        except Exception:
            pass
        for room in self.mongo.mongodb.room.find():
            try:
                self.oracle.cursor().execute(f"""
                    INSERT INTO ROOM VALUES (
                        {room['_id']},
                        {room['dormitory_id']},
                        {room['payment_amount']},
                        {room['room_number']},
                        {room['room_occupancy']},
                        {room['resident_registered']},
                        {format_date(room['disinfection_date'])}
                    )
                """)
            except Exception:
                pass

    def migrate_resident(self):
        try:
            self.oracle.cursor().execute("""
                CREATE TABLE resident(
                    ID NUMBER(5) PRIMARY KEY CHECK(ID>=0),
                    student NUMBER(5) NOT NULL CONSTRAINT resident_student_fkey REFERENCES STUDENT(ID),
                    room NUMBER(5) NOT NULL CONSTRAINT resident_room_fkey REFERENCES ROOM(ID),
                    visit_period_entered DATE,
                    visit_period_left DATE,
                    warnings_count NUMBER(5),
                    arrival_date DATE,
                    check_out_date DATE
                )
            """)
        except Exception:
            pass
        for resident in self.mongo.mongodb.resident.find():
            try:
                self.oracle.cursor().execute(f"""
                    INSERT INTO RESIDENT VALUES (
                        {resident['_id']},
                        {resident['student_id']},
                        {resident['room_id']},
                        {format_date(resident['visit_period_entered'])},
                        {format_date(resident['visit_period_left'])},
                        {resident['warnings_count']},
                        {format_date(resident['arrival_date'])},
                        {format_date(resident['check_out_date'])}
                    )
                """)
            except Exception:
                pass
