from migration.db_connection import DbConnections, format_date


class MysqlToAwm(DbConnections):

    def migrate(self):
        self.migrate_conference()
        self.migrate_library_card()
        self.migrate_pub_edition()
        self.migrate_publication()
        self.migrate_science_project()
        self.migrate_employee_conference()
        self.migrate_employee_publication()
        self.migrate_employee_science_project()

    def migrate_conference(self):
        print('mysql - migrating conferences')
        try:
            self.oracle.cursor().execute("""
                CREATE TABLE CONFERENCE(
                    ID NUMBER(5) PRIMARY KEY CHECK(ID>=0),
                    conference_name VARCHAR2(2000),
                    edition_location VARCHAR2(1000),
                    conference_date DATE
                )
            """)
        except Exception:
            pass
        mysql_cursor = self.mysql.cursor()
        mysql_cursor.execute('select * from CONFERENCE')
        for row in mysql_cursor:
            try:
                self.oracle.cursor().execute(f"""
                    INSERT INTO CONFERENCE VALUES (
                        {row['ID']}, 
                        '{row['CONFERENCE_NAME']}', 
                        '{row['EDITION_LOCATION']}', 
                        {format_date(row['CONFERENCE_DATE'])}
                    )
                """)
            except Exception:
                pass

    def migrate_library_card(self):
        print('mysql - migrating library cards')
        try:
            self.oracle.cursor().execute("""
                CREATE TABLE LIBRARY_CARD(
                    ID NUMBER(5) PRIMARY KEY CHECK(ID>=0),
                    reader NUMBER(5) NOT NULL CONSTRAINT lib_card_employee_fkey REFERENCES EMPLOYEE(ID),
                    book_name VARCHAR2(2000),
                    receipt_bool_date DATE,
                    return_book_date DATE NULL,
                    status_book VARCHAR2(1000)
                )
            """)
        except Exception:
            pass
        mysql_cursor = self.mysql.cursor()
        mysql_cursor.execute('select * from LIBRARY_CARD')
        for row in mysql_cursor:
            try:
                self.oracle.cursor().execute(f"""
                    INSERT INTO LIBRARY_CARD VALUES (
                        {row['ID']}, 
                        {row['READER']},
                        '{row['BOOK_NAME']}',
                        {format_date(row['RECEIPT_BOOL_DATE'])},
                        {format_date(row['RETURN_BOOK_DATE'])}, 
                        '{row['STATUS_BOOK']}'
                    )
                """)
            except Exception:
                pass

    def migrate_pub_edition(self):
        print('mysql - migrating pub_editions')
        try:
            self.oracle.cursor().execute("""
                CREATE TABLE PUB_EDITION(
                    ID NUMBER(5) PRIMARY KEY CHECK(ID>=0),
                    pub_edition_name VARCHAR2(2000),
                    pub_edition_language VARCHAR2(1000),
                    pub_edition_scope_sheets NUMBER(5),
                    pub_edition_location VARCHAR2(1000),
                    pub_edition_type VARCHAR2(1000)
                )
            """)
        except Exception:
            pass
        mysql_cursor = self.mysql.cursor()
        mysql_cursor.execute('select * from PUB_EDITION')
        for row in mysql_cursor:
            try:
                self.oracle.cursor().execute(f"""
                    INSERT INTO PUB_EDITION VALUES (
                        {row['ID']}, 
                        '{row['PUB_EDITION_NAME']}',
                        '{row['PUB_EDITION_LANGUAGE']}',
                        {row['PUB_EDITION_SCOPE_SHEETS']},
                        '{row['PUB_EDITION_LOCATION']}',
                        '{row['PUB_EDITION_TYPE']}'
                    )
                """)
            except Exception:
                pass

    def migrate_publication(self):
        print('mysql - migrating publications')
        try:
            self.oracle.cursor().execute("""
                CREATE TABLE PUBLICATION(
                    ID NUMBER(5) PRIMARY KEY CHECK(ID>=0),
                    publication_name VARCHAR2(2000),
                    citation_index NUMBER,
                    publication_date DATE,
                    pub_edition NUMBER(5) NOT NULL CONSTRAINT publication_edition_fkey REFERENCES PUB_EDITION(ID)
                )
            """)
        except Exception:
            pass
        mysql_cursor = self.mysql.cursor()
        mysql_cursor.execute('select * from PUBLICATION')
        for row in mysql_cursor:
            try:
                self.oracle.cursor().execute(f"""
                    INSERT INTO PUBLICATION VALUES (
                        {row['ID']}, 
                        '{row['PUBLICATION_NAME']}',
                        {row['CITATION_INDEX']},
                        {format_date(row['PUBLICATION_DATE'])},
                        {row['PUB_EDITION']}
                    )
                """)
            except Exception:
                pass

    def migrate_science_project(self):
        print('mysql - migrating general science projects')
        try:
            self.oracle.cursor().execute("""
                CREATE TABLE SCIENCE_PROJECT(
                    ID NUMBER(5) PRIMARY KEY CHECK(ID>=0),
                    science_project_name VARCHAR2(2000),
                    period_participation_start DATE,
                    period_participation_stop DATE
                )
            """)
        except Exception:
            pass
        mysql_cursor = self.mysql.cursor()
        mysql_cursor.execute('select * from SCIENCE_PROJECT')
        for row in mysql_cursor:
            try:
                self.oracle.cursor().execute(f"""
                    INSERT INTO SCIENCE_PROJECT VALUES (
                        {row['ID']}, 
                        '{row['SCIENCE_PROJECT_NAME']}',
                        {format_date(row['PERIOD_PARTICIPATION_START'])},
                        {format_date(row['PERIOD_PARTICIPATION_STOP'])}
                    )
                """)
            except Exception:
                pass

    def migrate_employee_conference(self):
        print('mysql - migrating employee conferences')
        try:
            self.oracle.cursor().execute("""
                CREATE TABLE EMPLOYEE_CONFERENCE(
                    ID NUMBER(5) PRIMARY KEY CHECK(ID>=0),
                    employee NUMBER(5) NOT NULL CONSTRAINT emp_conf_employee_fkey REFERENCES EMPLOYEE(ID),
                    conference NUMBER(5) NOT NULL CONSTRAINT emp_pub_conference_fkey REFERENCES CONFERENCE(ID)
                )
            """)
        except Exception:
            pass
        mysql_cursor = self.mysql.cursor()
        mysql_cursor.execute('select * from EMPLOYEE_CONFERENCE')
        for row in mysql_cursor:
            try:
                self.oracle.cursor().execute(f"""
                    INSERT INTO EMPLOYEE_CONFERENCE VALUES (
                        {row['ID']}, 
                        {row['EMPLOYEE']},
                        {row['CONFERENCE']}
                    )
                """)
            except Exception:
                pass

    def migrate_employee_publication(self):
        print('mysql - migrating employee publications')
        try:
            self.oracle.cursor().execute("""
                CREATE TABLE EMPLOYEE_PUBLICATION(
                    ID NUMBER(5) PRIMARY KEY CHECK(ID>=0),
                    employee NUMBER(5) NOT NULL CONSTRAINT emp_pub_employee_fkey REFERENCES EMPLOYEE(ID),
                    publication NUMBER(5) NOT NULL CONSTRAINT emp_pub_publication_fkey REFERENCES PUBLICATION(ID)
                )
            """)
        except Exception:
            pass
        mysql_cursor = self.mysql.cursor()
        mysql_cursor.execute('select * from EMPLOYEE_PUBLICATION')
        for row in mysql_cursor:
            try:
                self.oracle.cursor().execute(f"""
                    INSERT INTO EMPLOYEE_PUBLICATION VALUES (
                        {row['ID']}, 
                        {row['EMPLOYEE']},
                        {row['PUBLICATION']}
                    )
                """)
            except Exception:
                pass

    def migrate_employee_science_project(self):
        print('mysql - migrating employee science projects')
        try:
            self.oracle.cursor().execute("""
                CREATE TABLE EMPLOYEE_SCIENCE_PROJECT(
                    ID NUMBER(5) PRIMARY KEY CHECK(ID>=0),
                    employee NUMBER(5) NOT NULL CONSTRAINT emp_sci_pro_employee_fkey REFERENCES EMPLOYEE(ID),
                    science_project NUMBER(5) NOT NULL CONSTRAINT emp_sci_pro_science_project_fkey REFERENCES SCIENCE_PROJECT(ID)
                )
            """)
        except Exception:
            pass
        mysql_cursor = self.mysql.cursor()
        mysql_cursor.execute('select * from EMPLOYEE_SCIENCE_PROJECT')
        for row in mysql_cursor:
            try:
                self.oracle.cursor().execute(f"""
                    INSERT INTO EMPLOYEE_SCIENCE_PROJECT VALUES (
                        {row['ID']}, 
                        {row['EMPLOYEE']},
                        {row['SCIENCE_PROJECT']}
                    )
                """)
            except Exception:
                pass
