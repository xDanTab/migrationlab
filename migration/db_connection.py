import psycopg2
import pymysql
import cx_Oracle

from pymongo import MongoClient
from pymysql.cursors import DictCursor


def format_date(field):
    if field is None:
        return "to_date(null, 'yyyy/mm/dd')"
    return f"to_date('{field.strftime('%Y/%m/%d')}', 'yyyy/mm/dd')"


def connect_to_mongodb():
    print('connecting to mongodb')
    return MongoClient('mongodb://root:something_weird@localhost:27017/mongodb')


def connect_to_mysql():
    print('connecting to mysql')
    return pymysql.connect(
        host='localhost',
        port=3306,
        user='root',
        password='something_weird',
        db='mysqldb',
        cursorclass=DictCursor
    )


def connect_to_postgres():
    print('connecting to postgres')
    return psycopg2.connect(
        dbname='pg_database',
        user='root',
        password='something_weird',
        host='localhost',
        port=5432,
    )


def connect_to_oracle():
    print('connecting to oracle')
    ORACLE_LIB_DIR = 'C:/Users/BDT/PycharmProjects/migrationlab/migration/instantclient_19_12'
    try:
        cx_Oracle.init_oracle_client(lib_dir=ORACLE_LIB_DIR)
    except Exception as e:
        if str(e) != 'Oracle Client library has already been initialized':
            raise e
    oracle = cx_Oracle.connect(u'UNIVERSITY/something_weird@localhost:1521/XE')
    oracle.autocommit = True
    return oracle


class DbConnections:
    def __init__(self):
        self.mongo = connect_to_mongodb()
        self.oracle = connect_to_oracle()
        self.mysql = connect_to_mysql()
        self.postgres = connect_to_postgres()
