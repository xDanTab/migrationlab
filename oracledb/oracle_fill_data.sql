INSERT INTO EMPLOYEE
VALUES (1, 'Calhoun', 'Joshua', 'Gaskins', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (2, 'Bright', 'Tisha', 'Wright', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (3, 'Beltran', 'Everette', 'Llamas', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (4, 'Dempsey', 'Thomas', 'Ryder', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (5, 'Duberry', 'Janet', 'Miller', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (6, 'Dykes', 'Jamie', 'States', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (7, 'Raman', 'April', 'Wagoner', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (8, 'Troyer', 'William', 'Ventura', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (9, 'Collins', 'Brittany', 'Holley', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (10, 'Talamo', 'David', 'Martin', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (11, 'Owen', 'Richard', 'Church', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (12, 'Cook', 'William', 'Toney', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (13, 'Maloney', 'Sharen', 'Roddam', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (14, 'Arwood', 'Douglas', 'Werra', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (15, 'Kovacs', 'Brian', 'Richardson', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (16, 'Austin', 'James', 'Byun', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (17, 'Rubin', 'Charles', 'Pierre', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (18, 'Lynch', 'Cheryl', 'Lopez', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (19, 'Aviles', 'James', 'Beales', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (20, 'Tagliarini', 'Allison', 'Binkley', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (21, 'Strong', 'Willis', 'Mayes', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (22, 'Greenwood', 'Michael', 'Morgan', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (23, 'Frazier', 'Robert', 'Vanpelt', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (24, 'Borrero', 'Wm', 'Mcguire', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (25, 'Anderson', 'Doreen', 'Bell', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (26, 'Molon', 'Anne', 'Lozon', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (27, 'Barlow', 'Beulah', 'Vanderloo', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (28, 'Benson', 'James', 'Tindell', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (29, 'Bohne', 'Donald', 'Lay', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (30, 'Warren', 'Teresa', 'Coleman', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (31, 'Placha', 'Neal', 'Souza', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (32, 'Hawkins', 'Richard', 'Brewer', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (33, 'Dixon', 'Heriberto', 'Prohaska', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (34, 'Holton', 'Jessie', 'Jensen', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (35, 'Fuller', 'John', 'Rogers', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (36, 'Freeman', 'Melissa', 'Ortiz', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (37, 'Forshey', 'Terry', 'Souza', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (38, 'Lilly', 'Mike', 'Lynch', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (39, 'Shook', 'Fernando', 'Starns', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (40, 'Everhardt', 'Joseph', 'Kline', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (41, 'Cobham', 'Patricia', 'Brewer', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (42, 'Vogt', 'Kenneth', 'Cheng', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (43, 'Page', 'Susan', 'Crowley', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (44, 'Hicks', 'Ashlyn', 'Sparks', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (45, 'Thompson', 'Marjorie', 'Lesher', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (46, 'Harris', 'Patrick', 'Alexander', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (47, 'William', 'Anne', 'Gillum', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (48, 'Pope', 'Maurice', 'Nichols', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (49, 'Clay', 'Dorothy', 'Ramer', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (50, 'Martinez', 'Carly', 'Haley', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (51, 'Lewis', 'Betty', 'Klein', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (52, 'Asmus', 'Roger', 'Patton', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (53, 'Morales', 'Joseph', 'Hernandez', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (54, 'Lucas', 'Jerry', 'Hegwood', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (55, 'Martinez', 'Theodore', 'Gurganus', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (56, 'Howard', 'Dorothy', 'Perry', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (57, 'Griggs', 'Kevin', 'Sellers', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (58, 'Thomas', 'Leroy', 'Mimms', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (59, 'Bennet', 'Lora', 'Deen', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (60, 'Dennehy', 'Jeffrey', 'Heinz', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (61, 'Bartley', 'Lewis', 'Himmel', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (62, 'Maxwell', 'Rhonda', 'York', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (63, 'Turner', 'Pilar', 'Scott', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (64, 'Dixon', 'Gerald', 'Watson', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (65, 'Brooks', 'Ian', 'Lynch', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (66, 'Page', 'Betty', 'Condon', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (67, 'Kemnitz', 'Jamie', 'Kirby', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (68, 'Walkingstick', 'Louis', 'Johnson', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (69, 'Suniga', 'Joseph', 'Hare', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (70, 'Webster', 'Billy', 'Scott', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (71, 'Rudge', 'Kristina', 'Anderson', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (72, 'Gandolfi', 'Brooke', 'Catalano', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (73, 'Holcomb', 'Robin', 'Morris', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (74, 'Sweet', 'Anna', 'Loch', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (75, 'Jones', 'James', 'Boldenow', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (76, 'Livings', 'Tony', 'Howard', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (77, 'Bradshaw', 'Carl', 'Smith', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (78, 'Wang', 'Dennis', 'Adams', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (79, 'Green', 'Matthew', 'Roman', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (80, 'Sandau', 'Joanna', 'Clark', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (81, 'Bearden', 'Joel', 'Davenport', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (82, 'Lilly', 'Helen', 'Moses', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (83, 'Zordan', 'Michael', 'Asbell', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (84, 'Wilson', 'Leslie', 'Punches', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (85, 'Finn', 'James', 'Carter', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (86, 'Anderson', 'Anthony', 'Beane', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (87, 'Yoder', 'Earl', 'Hill', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (88, 'Blake', 'David', 'Gallegos', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (89, 'Almond', 'Robert', 'Kieser', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (90, 'Robinson', 'Tad', 'Benton', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (91, 'Capulong', 'Jason', 'Ronning', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (92, 'Miller', 'Brandi', 'Burris', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (93, 'Hall', 'Alta', 'Mcneil', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (94, 'Miller', 'Michelle', 'Gaylord', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (95, 'Weber', 'Lillie', 'Robinette', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (96, 'Cepero', 'Paulette', 'Piccinone', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (97, 'Edwards', 'Denise', 'Deaner', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (98, 'Jaime', 'David', 'Torres', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (99, 'Alexander', 'Michael', 'Trexler', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр',
        'ПИиКТ', to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));
INSERT INTO EMPLOYEE
VALUES (100, 'Prado', 'Scott', 'Grossi', to_date('1998/01/10', 'yyyy/mm/dd'), 'Санкт-Петербург', 'Бакалавр', 'ПИиКТ',
        to_date('2019/09/01', 'yyyy/mm/dd'), to_date('2021/08/31', 'yyyy/mm/dd'));


INSERT INTO STUDY_GROUP
VALUES (1, 3, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (2, 1, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (3, 3, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (4, 3, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (5, 4, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (6, 4, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (7, 4, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (8, 1, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (9, 3, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (10, 2, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (11, 3, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (12, 2, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (13, 1, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (14, 1, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (15, 3, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (16, 1, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (17, 2, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (18, 3, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (19, 2, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (20, 4, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (21, 3, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (22, 2, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (23, 4, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (24, 1, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));
INSERT INTO STUDY_GROUP
VALUES (25, 2, to_date('2021/09/01', 'yyyy/mm/dd'), to_date('2022/08/31', 'yyyy/mm/dd'));

INSERT INTO STUDENT
VALUES (1, 1, 3, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (2, 2, 1, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (3, 3, 16, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (4, 4, 24, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (5, 5, 3, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (6, 6, 15, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (7, 7, 7, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (8, 8, 3, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (9, 9, 24, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (10, 10, 23, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (11, 11, 25, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (12, 12, 25, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (13, 13, 10, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (14, 14, 2, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (15, 15, 23, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (16, 16, 19, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (17, 17, 18, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (18, 18, 19, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (19, 19, 21, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (20, 20, 10, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (21, 21, 9, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (22, 22, 16, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (23, 23, 8, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (24, 24, 25, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (25, 25, 1, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (26, 26, 19, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (27, 27, 2, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (28, 28, 5, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (29, 29, 25, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (30, 30, 22, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (31, 31, 16, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (32, 32, 11, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (33, 33, 4, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (34, 34, 6, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (35, 35, 19, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (36, 36, 24, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (37, 37, 23, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (38, 38, 14, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (39, 39, 19, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (40, 40, 16, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (41, 41, 10, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (42, 42, 21, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (43, 43, 11, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (44, 44, 9, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (45, 45, 17, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (46, 46, 11, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (47, 47, 6, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (48, 48, 1, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (49, 49, 20, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (50, 50, 23, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (51, null, 1, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (52, null, 19, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (53, null, 20, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (54, null, 21, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (55, null, 8, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (56, null, 11, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (57, null, 21, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (58, null, 23, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (59, null, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (60, null, 17, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (61, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (62, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (63, NULL, 8, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (64, NULL, 8, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (65, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (66, NULL, 21, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (67, NULL, 21, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (68, NULL, 24, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (69, NULL, 8, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (70, NULL, 11, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (71, NULL, 22, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (72, NULL, 11, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (73, NULL, 15, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (74, NULL, 13, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (75, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (76, NULL, 21, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (77, NULL, 21, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (78, NULL, 12, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (79, NULL, 14, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (80, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (81, NULL, 11, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (82, NULL, 10, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (83, NULL, 23, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (84, NULL, 9, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (85, NULL, 1, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (86, NULL, 23, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (87, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (88, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (89, NULL, 23, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (90, NULL, 21, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (91, NULL, 10, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (92, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (93, NULL, 7, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (94, NULL, 13, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (95, NULL, 19, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (96, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (97, NULL, 8, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (98, NULL, 17, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (99, NULL, 9, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (100, NULL, 21, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (101, NULL, 1, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (102, NULL, 21, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (103, NULL, 8, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (104, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (105, NULL, 12, 'заочная', 'Программирование и интернет-технологии', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (106, NULL, 5, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (107, NULL, 22, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (108, NULL, 21, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (109, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (110, NULL, 9, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (111, NULL, 1, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (112, NULL, 18, 'заочная', 'Программирование и интернет-технологии', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (113, NULL, 3, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (114, NULL, 1, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (115, NULL, 22, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (116, NULL, 11, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (117, NULL, 11, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (118, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (119, NULL, 1, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (120, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (121, NULL, 14, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (122, NULL, 14, 'заочная', 'Программирование и интернет-технологии', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (123, NULL, 21, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (124, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (125, NULL, 14, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (126, NULL, 7, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (127, NULL, 7, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (128, NULL, 21, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (129, NULL, 11, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (130, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (131, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (132, NULL, 1, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (133, NULL, 10, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (134, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (135, NULL, 10, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (136, NULL, 18, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (137, NULL, 21, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (138, NULL, 21, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (139, NULL, 23, 'заочная', 'Программирование и интернет-технологии', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (140, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (141, NULL, 19, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (142, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (143, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (144, NULL, 10, 'заочная', 'Программирование и интернет-технологии', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (145, NULL, 21, 'заочная', 'Программирование и интернет-технологии', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (146, NULL, 14, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (147, NULL, 21, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (148, NULL, 18, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (149, NULL, 11, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (150, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (151, NULL, 25, 'заочная', 'Программирование и интернет-технологии', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (152, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (153, NULL, 21, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (154,NULL, 2, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (155, NULL, 11, 'заочная', 'Программирование и интернет-технологии', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (156, NULL, 18, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (157, NULL, 4, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (158, NULL, 12, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (159, NULL, 20, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (160, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (161, NULL, 20, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (162, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (163, NULL, 9, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (164, NULL, 1, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (165, NULL, 16, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (166, NULL, 2, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (167, NULL, 9, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (168, NULL, 7, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (169, NULL, 19, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (170, NULL, 14, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (171, NULL, 8, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (172, NULL, 22, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (173, NULL, 17, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (174, NULL, 10, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (175, NULL, 15, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (176, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (177, NULL, 10, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (178, NULL, 15, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (179, NULL, 16, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (180, NULL, 7, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (181, NULL, 2, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (182, NULL, 22, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (183, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (184, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (185, NULL, 24, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (186, NULL, 15, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (187, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (188, NULL, 17, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (189, NULL, 11, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (190, NULL, 10, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (191, NULL, 21, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (192, NULL, 21, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (193, NULL, 19, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (194, NULL, 11, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (195, NULL, 11, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (196, NULL, 21, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (197, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (198, NULL, 7, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (199, NULL, 14, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (200, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (201, NULL, 1, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (202, NULL, 24, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (203, NULL, 11, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (204, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (205, NULL, 2, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (206, NULL, 11, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (207, NULL, 20, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (208, NULL, 5, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (209, NULL, 10, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (210, NULL, 1, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (211, NULL, 16, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (212, NULL, 24, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (213, NULL, 24, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (214, NULL, 9, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (215, NULL, 4, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (216, NULL, 5, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (217, NULL, 12, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (218, NULL, 11, 'заочная', 'Программирование и интернет-технологии', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (219, NULL, 3, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (220, NULL, 1, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (221, NULL, 21, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (222, NULL, 18, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (223, NULL, 9, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (224, NULL, 11, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (225, NULL, 22, 'заочная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (226, NULL, 15, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (227, NULL, 21, 'очная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (228, NULL, 1, 'заочная', 'Программирование и интернет-технологии', 'контракт', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (229, NULL, 22, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (230, NULL, 11, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (231, NULL, 13, 'заочная', 'Программирование и интернет-технологии', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (232, NULL, 19, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (233, NULL, 7, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (234, NULL, 1, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (235, NULL, 21, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (236, NULL, 18, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (237, NULL, 15, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (238, NULL, 17, 'заочная', 'Программирование и интернет-технологии', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (239, NULL, 20, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (240, NULL, 20, 'заочная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (241, NULL, 1, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (242, NULL, 21, 'очная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (243, NULL, 10, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (244, NULL, 17, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (245,NULL, 6, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (246, NULL, 9, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (247, NULL, 13, 'заочная', 'Программирование и интернет-технологии', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (248, NULL, 17, 'очная', 'Системное и прикладное программное обеспечение', 'контракт',
        '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (249, NULL, 21, 'очная', 'Программирование и интернет-технологии', 'бюджет', '09.04.04 Программная инженерия');
INSERT INTO STUDENT
VALUES (250, NULL, 18, 'заочная', 'Системное и прикладное программное обеспечение', 'бюджет',
        '09.04.04 Программная инженерия');


INSERT INTO DISCIPLINE
VALUES (1, 'Распределенные базы данных и знаний', '2020/2021');
INSERT INTO DISCIPLINE
VALUES (2, 'Методология программной инженерии', '2020/2021');
INSERT INTO DISCIPLINE
VALUES (3, 'Методы машинного обучения', '2020/2021');

INSERT INTO ACADEMIC_PERFORMANCE
VALUES (1, 3, 56, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (2, 2, 63, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (3, 1, 82, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (4, 1, 92, to_date('2021/03/25', 'yyyy/mm/dd'), 'D');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (5, 2, 83, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (6, 1, 81, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (7, 3, 71, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (8, 2, 69, to_date('2021/03/25', 'yyyy/mm/dd'), 'F');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (9, 3, 59, to_date('2021/03/25', 'yyyy/mm/dd'), 'D');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (10, 1, 90, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (11, 1, 78, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (12, 2, 87, to_date('2021/03/25', 'yyyy/mm/dd'), 'D');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (13, 1, 99, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (14, 2, 77, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (15, 1, 69, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (16, 1, 73, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (17, 3, 66, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (18, 3, 55, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (19, 3, 90, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (20, 3, 62, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (21, 1, 70, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (22, 2, 58, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (23, 2, 58, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (24, 3, 84, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (25, 2, 97, to_date('2021/03/25', 'yyyy/mm/dd'), 'D');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (26, 2, 98, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (27, 2, 84, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (28, 2, 84, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (29, 2, 80, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (30, 2, 85, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (31, 1, 93, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (32, 1, 84, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (33, 2, 88, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (34, 2, 61, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (35, 1, 79, to_date('2021/03/25', 'yyyy/mm/dd'), 'D');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (36, 3, 66, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (37, 1, 69, to_date('2021/03/25', 'yyyy/mm/dd'), 'D');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (38, 2, 74, to_date('2021/03/25', 'yyyy/mm/dd'), 'F');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (39, 2, 98, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (40, 3, 74, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (41, 2, 94, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (42, 1, 85, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (43, 3, 69, to_date('2021/03/25', 'yyyy/mm/dd'), 'F');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (44, 2, 59, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (45, 2, 81, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (46, 1, 92, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (47, 3, 99, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (48, 1, 97, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (49, 3, 70, to_date('2021/03/25', 'yyyy/mm/dd'), 'F');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (50, 2, 74, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (51, 2, 67, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (52, 2, 79, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (53, 2, 55, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (54, 1, 74, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (55, 3, 77, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (56, 3, 57, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (57, 1, 76, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (58, 3, 93, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (59, 1, 98, to_date('2021/03/25', 'yyyy/mm/dd'), 'F');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (60, 2, 96, to_date('2021/03/25', 'yyyy/mm/dd'), 'F');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (61, 1, 63, to_date('2021/03/25', 'yyyy/mm/dd'), 'D');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (62, 1, 60, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (63, 3, 61, to_date('2021/03/25', 'yyyy/mm/dd'), 'F');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (64, 1, 87, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (65, 3, 95, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (66, 2, 100, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (67, 3, 71, to_date('2021/03/25', 'yyyy/mm/dd'), 'F');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (68, 3, 78, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (69, 3, 73, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (70, 3, 88, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (71, 3, 78, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (72, 2, 70, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (73, 2, 84, to_date('2021/03/25', 'yyyy/mm/dd'), 'F');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (74, 3, 93, to_date('2021/03/25', 'yyyy/mm/dd'), 'F');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (75, 3, 73, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (76, 2, 84, to_date('2021/03/25', 'yyyy/mm/dd'), 'D');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (77, 2, 87, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (78, 1, 93, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (79, 1, 93, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (80, 3, 87, to_date('2021/03/25', 'yyyy/mm/dd'), 'D');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (81, 1, 60, to_date('2021/03/25', 'yyyy/mm/dd'), 'D');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (82, 2, 87, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (83, 1, 69, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (84, 3, 75, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (85, 2, 81, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (86, 3, 78, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (87, 1, 97, to_date('2021/03/25', 'yyyy/mm/dd'), 'C');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (88, 2, 62, to_date('2021/03/25', 'yyyy/mm/dd'), 'D');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (89, 3, 73, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (90, 1, 94, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (91, 2, 95, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (92, 2, 68, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (93, 1, 89, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (94, 1, 86, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (95, 3, 79, to_date('2021/03/25', 'yyyy/mm/dd'), 'D');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (96, 3, 81, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (97, 2, 80, to_date('2021/03/25', 'yyyy/mm/dd'), 'E');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (98, 2, 83, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (99, 1, 90, to_date('2021/03/25', 'yyyy/mm/dd'), 'B');
INSERT INTO ACADEMIC_PERFORMANCE
VALUES (100, 2, 95, to_date('2021/03/25', 'yyyy/mm/dd'), 'A');

INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (1, 7, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (2, 37, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (3, 15, 1);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (4, 19, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (5, 27, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (6, 41, 2);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (7, 5, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (8, 34, 2);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (9, 9, 2);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (10, 87, 2);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (11, 44, 1);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (12, 17, 1);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (13, 68, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (14, 47, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (15, 10, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (16, 38, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (17, 64, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (18, 12, 2);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (19, 23, 1);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (20, 57, 1);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (21, 58, 2);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (22, 76, 2);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (23, 21, 1);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (24, 31, 2);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (25, 28, 2);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (26, 51, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (27, 34, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (28, 32, 2);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (29, 28, 3);
INSERT INTO EMPLOYEE_DISCIPLINE
VALUES (30, 35, 2);

INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (1, 1, 1);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (2, 2, 2);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (3, 3, 3);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (4, 4, 4);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (5, 5, 5);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (6, 6, 6);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (7, 7, 7);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (8, 8, 8);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (9, 9, 9);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (10, 10, 10);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (11, 11, 11);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (12, 12, 12);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (13, 13, 13);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (14, 14, 14);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (15, 15, 15);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (16, 16, 16);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (17, 17, 17);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (18, 18, 18);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (19, 19, 19);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (20, 20, 20);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (21, 21, 21);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (22, 22, 22);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (23, 23, 23);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (24, 24, 24);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (25, 25, 25);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (26, 26, 26);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (27, 27, 27);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (28, 28, 28);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (29, 29, 29);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (30, 30, 30);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (31, 31, 31);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (32, 32, 32);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (33, 33, 33);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (34, 34, 34);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (35, 35, 35);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (36, 36, 36);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (37, 37, 37);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (38, 38, 38);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (39, 39, 39);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (40, 40, 40);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (41, 41, 41);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (42, 42, 42);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (43, 43, 43);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (44, 44, 44);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (45, 45, 45);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (46, 46, 46);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (47, 47, 47);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (48, 48, 48);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (49, 49, 49);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (50, 50, 50);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (51, 51, 51);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (52, 52, 52);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (53, 53, 53);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (54, 54, 54);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (55, 55, 55);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (56, 56, 56);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (57, 57, 57);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (58, 58, 58);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (59, 59, 59);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (60, 60, 60);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (61, 61, 61);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (62, 62, 62);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (63, 63, 63);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (64, 64, 64);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (65, 65, 65);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (66, 66, 66);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (67, 67, 67);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (68, 68, 68);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (69, 69, 69);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (70, 70, 70);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (71, 71, 71);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (72, 72, 72);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (73, 73, 73);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (74, 74, 74);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (75, 75, 75);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (76, 76, 76);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (77, 77, 77);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (78, 78, 78);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (79, 79, 79);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (80, 80, 80);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (81, 81, 81);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (82, 82, 82);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (83, 83, 83);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (84, 84, 84);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (85, 85, 85);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (86, 86, 86);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (87, 87, 87);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (88, 88, 88);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (89, 89, 89);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (90, 90, 90);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (91, 91, 91);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (92, 92, 92);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (93, 93, 93);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (94, 94, 94);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (95, 95, 95);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (96, 96, 96);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (97, 97, 97);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (98, 98, 98);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (99, 99, 99);
INSERT INTO STUDENT_ACADEMIC_PERFORMANCE
VALUES (100, 100, 100);

INSERT INTO SCHEDULE
VALUES (1, 1, to_date('2021/07/04 09:00', 'yyyy/mm/dd hh24:mi'), 693);
INSERT INTO SCHEDULE
VALUES (2, 2, to_date('2021/03/04 08:00', 'yyyy/mm/dd hh24:mi'), 1031);
INSERT INTO SCHEDULE
VALUES (3, 3, to_date('2021/01/04 09:00', 'yyyy/mm/dd hh24:mi'), 772);
INSERT INTO SCHEDULE
VALUES (4, 3, to_date('2021/09/04 09:00', 'yyyy/mm/dd hh24:mi'), 1453);
INSERT INTO SCHEDULE
VALUES (5, 1, to_date('2021/03/04 09:00', 'yyyy/mm/dd hh24:mi'), 1145);
INSERT INTO SCHEDULE
VALUES (6, 3, to_date('2021/04/04 08:00', 'yyyy/mm/dd hh24:mi'), 716);
INSERT INTO SCHEDULE
VALUES (7, 1, to_date('2021/05/04 08:00', 'yyyy/mm/dd hh24:mi'), 971);
INSERT INTO SCHEDULE
VALUES (8, 1, to_date('2021/02/04 09:00', 'yyyy/mm/dd hh24:mi'), 1026);
INSERT INTO SCHEDULE
VALUES (9, 2, to_date('2021/05/04 08:00', 'yyyy/mm/dd hh24:mi'), 570);
INSERT INTO SCHEDULE
VALUES (10, 1, to_date('2021/02/04 09:00', 'yyyy/mm/dd hh24:mi'), 751);
INSERT INTO SCHEDULE
VALUES (11, 2, to_date('2021/04/04 08:00', 'yyyy/mm/dd hh24:mi'), 471);
INSERT INTO SCHEDULE
VALUES (12, 3, to_date('2021/06/04 08:00', 'yyyy/mm/dd hh24:mi'), 310);
INSERT INTO SCHEDULE
VALUES (13, 3, to_date('2021/09/04 09:00', 'yyyy/mm/dd hh24:mi'), 557);
INSERT INTO SCHEDULE
VALUES (14, 2, to_date('2021/07/04 08:00', 'yyyy/mm/dd hh24:mi'), 665);
INSERT INTO SCHEDULE
VALUES (15, 2, to_date('2021/06/04 09:00', 'yyyy/mm/dd hh24:mi'), 1179);
INSERT INTO SCHEDULE
VALUES (16, 3, to_date('2021/04/04 09:00', 'yyyy/mm/dd hh24:mi'), 587);
INSERT INTO SCHEDULE
VALUES (17, 3, to_date('2021/02/04 09:00', 'yyyy/mm/dd hh24:mi'), 712);
INSERT INTO SCHEDULE
VALUES (18, 3, to_date('2021/09/04 08:00', 'yyyy/mm/dd hh24:mi'), 568);
INSERT INTO SCHEDULE
VALUES (19, 1, to_date('2021/05/04 09:00', 'yyyy/mm/dd hh24:mi'), 517);
INSERT INTO SCHEDULE
VALUES (20, 2, to_date('2021/05/04 08:00', 'yyyy/mm/dd hh24:mi'), 1120);
INSERT INTO SCHEDULE
VALUES (21, 3, to_date('2021/08/04 08:00', 'yyyy/mm/dd hh24:mi'), 1135);
INSERT INTO SCHEDULE
VALUES (22, 2, to_date('2021/01/04 08:00', 'yyyy/mm/dd hh24:mi'), 1250);
INSERT INTO SCHEDULE
VALUES (23, 2, to_date('2021/01/04 09:00', 'yyyy/mm/dd hh24:mi'), 540);
INSERT INTO SCHEDULE
VALUES (24, 2, to_date('2021/03/04 08:00', 'yyyy/mm/dd hh24:mi'), 175);
INSERT INTO SCHEDULE
VALUES (25, 2, to_date('2021/05/04 08:00', 'yyyy/mm/dd hh24:mi'), 1323);
INSERT INTO SCHEDULE
VALUES (26, 1, to_date('2021/07/04 09:00', 'yyyy/mm/dd hh24:mi'), 436);
INSERT INTO SCHEDULE
VALUES (27, 2, to_date('2021/06/04 09:00', 'yyyy/mm/dd hh24:mi'), 260);
INSERT INTO SCHEDULE
VALUES (28, 1, to_date('2021/06/04 08:00', 'yyyy/mm/dd hh24:mi'), 1087);
INSERT INTO SCHEDULE
VALUES (29, 1, to_date('2021/05/04 08:00', 'yyyy/mm/dd hh24:mi'), 209);
INSERT INTO SCHEDULE
VALUES (30, 2, to_date('2021/07/04 09:00', 'yyyy/mm/dd hh24:mi'), 1336);
INSERT INTO SCHEDULE
VALUES (31, 2, to_date('2021/07/04 08:00', 'yyyy/mm/dd hh24:mi'), 1138);
INSERT INTO SCHEDULE
VALUES (32, 1, to_date('2021/01/04 09:00', 'yyyy/mm/dd hh24:mi'), 909);
INSERT INTO SCHEDULE
VALUES (33, 3, to_date('2021/05/04 09:00', 'yyyy/mm/dd hh24:mi'), 584);
INSERT INTO SCHEDULE
VALUES (34, 1, to_date('2021/07/04 08:00', 'yyyy/mm/dd hh24:mi'), 336);
INSERT INTO SCHEDULE
VALUES (35, 3, to_date('2021/01/04 08:00', 'yyyy/mm/dd hh24:mi'), 770);
INSERT INTO SCHEDULE
VALUES (36, 3, to_date('2021/07/04 08:00', 'yyyy/mm/dd hh24:mi'), 873);
INSERT INTO SCHEDULE
VALUES (37, 2, to_date('2021/07/04 09:00', 'yyyy/mm/dd hh24:mi'), 476);
INSERT INTO SCHEDULE
VALUES (38, 1, to_date('2021/05/04 08:00', 'yyyy/mm/dd hh24:mi'), 343);
INSERT INTO SCHEDULE
VALUES (39, 3, to_date('2021/07/04 09:00', 'yyyy/mm/dd hh24:mi'), 638);
INSERT INTO SCHEDULE
VALUES (40, 2, to_date('2021/09/04 09:00', 'yyyy/mm/dd hh24:mi'), 479);
INSERT INTO SCHEDULE
VALUES (41, 3, to_date('2021/01/04 08:00', 'yyyy/mm/dd hh24:mi'), 551);
INSERT INTO SCHEDULE
VALUES (42, 2, to_date('2021/07/04 08:00', 'yyyy/mm/dd hh24:mi'), 370);
INSERT INTO SCHEDULE
VALUES (43, 1, to_date('2021/07/04 08:00', 'yyyy/mm/dd hh24:mi'), 946);
INSERT INTO SCHEDULE
VALUES (44, 1, to_date('2021/09/04 08:00', 'yyyy/mm/dd hh24:mi'), 670);
INSERT INTO SCHEDULE
VALUES (45, 1, to_date('2021/02/04 09:00', 'yyyy/mm/dd hh24:mi'), 644);
INSERT INTO SCHEDULE
VALUES (46, 3, to_date('2021/06/04 08:00', 'yyyy/mm/dd hh24:mi'), 243);
INSERT INTO SCHEDULE
VALUES (47, 2, to_date('2021/03/04 08:00', 'yyyy/mm/dd hh24:mi'), 743);
INSERT INTO SCHEDULE
VALUES (48, 1, to_date('2021/09/04 09:00', 'yyyy/mm/dd hh24:mi'), 1360);
INSERT INTO SCHEDULE
VALUES (49, 2, to_date('2021/09/04 09:00', 'yyyy/mm/dd hh24:mi'), 932);
INSERT INTO SCHEDULE
VALUES (50, 3, to_date('2021/03/04 09:00', 'yyyy/mm/dd hh24:mi'), 1143);
