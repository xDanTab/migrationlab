from migration.mysql_to_awm import MysqlToAwm
from migration.postgres_to_awm import PostgresToAwm
from migration.mongo_to_awm import MongoToAwm

if __name__ == '__main__':
    print('migration has started')
    PostgresToAwm().migrate()
    MysqlToAwm().migrate()
    MongoToAwm().migrate()
    print('migration has ended')

