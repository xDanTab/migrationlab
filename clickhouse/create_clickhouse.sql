CREATE TABLE student
(
    `student_id`              UInt32,
    `employee_id`             UInt32,
    `study_group`             UInt32,
    `education_form`          String,
    `specialty`               String,
    `education_type`          String,
    `direction`               String,
    `education_standard_type` String,
    `has_benefits`            String
) ENGINE ReplacingMergeTree(student_id) ORDER BY student_id PRIMARY KEY student_id;
create table resident
(
    `id`                   UInt32,
    `student`              UInt32,
    `room`                 UInt32,
    `visit_period_entered` Date,
    `visit_period_left`    Date,
    `warnings_count`       UInt32,
    `arrival_date`         Date,
    `check_out_date`       Date
) ENGINE ReplacingMergeTree(id) ORDER BY id PRIMARY KEY id;

create table employee
(
    `id`                UInt32,
    `lastname`          String,
    `firstname`         String,
    `patronymicname`    String,
    `birth_Date`        Date,
    `birth_place`       String,
    `position`          String,
    `subdivision`       String,
    `work_start_period` Date,
    `work_stop_period`  Date,
    `university_name`   String
) ENGINE ReplacingMergeTree(id) ORDER BY id PRIMARY KEY id;

create table academic_performance
(
    `id`           UInt32,
    `discipline`   UInt32,
    `points`       UInt32,
    `grading_date` Date,
    `letter`       String
) ENGINE ReplacingMergeTree(id) ORDER BY id PRIMARY KEY id;
create table student_academic_performance
(
    `id`                   UInt32,
    `student`              UInt32,
    `personal_performance` UInt32
) ENGINE ReplacingMergeTree(id) ORDER BY id PRIMARY KEY id;
create table discipline
(
    `id`              UInt32,
    `discipline_name` String,
    `academic_year`   String,
    `semester`        UInt32,
    `lectures_hours`  UInt32,
    `practice_hours`  UInt32,
    `lab_hours`       UInt32,
    `control_form`    String
) ENGINE ReplacingMergeTree(id) ORDER BY id PRIMARY KEY id;
create table pub_edition
(
    id                       UInt32,
    pub_edition_name         String,
    pub_edition_language     String,
    pub_edition_scope_sheets UInt32,
    pub_edition_location     String,
    pub_edition_type         String
) ENGINE ReplacingMergeTree(id) ORDER BY id PRIMARY KEY id;
create table publication
(
    id               UInt32,
    publication_name String,
    citation_index   UInt32,
    publication_Date Date,
    pub_edition      UInt32
) ENGINE ReplacingMergeTree(id) ORDER BY id PRIMARY KEY id;

create table employee_publication
(
    id          UInt32,
    employee    UInt32,
    publication UInt32
) ENGINE ReplacingMergeTree(id) ORDER BY id PRIMARY KEY id;
